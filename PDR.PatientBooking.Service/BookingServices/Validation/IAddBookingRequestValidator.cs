﻿using PDR.PatientBooking.Data.Models;
using PDR.PatientBooking.Service.PatientServices.Requests;
using PDR.PatientBooking.Service.Validation;
using System.Collections.Generic;

namespace PDR.PatientBooking.Service.PatientServices.Validation
{
    public interface IAddBookingRequestValidator
    {
        PdrValidationResult ValidateRequest(AddBookingRequest request);
        PdrValidationResult ValidateNextAppointmentRequest(List<Order> bookings, long identificationNumber);
    }
}