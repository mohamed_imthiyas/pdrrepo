﻿using PDR.PatientBooking.Data;
using PDR.PatientBooking.Data.Models;
using PDR.PatientBooking.Service.PatientServices.Requests;
using PDR.PatientBooking.Service.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PDR.PatientBooking.Service.PatientServices.Validation
{
    public class AddBookingRequestValidator : IAddBookingRequestValidator
    {
        private readonly PatientBookingContext _context;

        public AddBookingRequestValidator(PatientBookingContext context)
        {
            _context = context;
        }

        public PdrValidationResult ValidateNextAppointmentRequest(List<Order> bookings, long patientId)
        {
            var result = new PdrValidationResult(true);
            if (PatientNotFound(bookings, patientId, ref result))
                return result;
            if (FutureBookingNotFound(bookings, patientId, ref result))
                return result;
            return result;
        }

        public PdrValidationResult ValidateRequest(AddBookingRequest request)
        {
            var result = new PdrValidationResult(true);

            BookingNotDoneInPast(request, ref result);
            BookingNotOverlaps(request, ref result);

            return result;
        }
        private bool PatientNotFound(List<Order> bookings, long patientId, ref PdrValidationResult result)
        {
            if (bookings.Where(x => x.Patient.Id == patientId).Count() == 0)
            {
                result.PassedValidation = false;
                result.Errors.Add("Booking is not found for the patient!");
                return true;
            }

            return false;
        }
        private bool FutureBookingNotFound(List<Order> bookings, long patientId, ref PdrValidationResult result)
        {
            if (bookings.Where(x => x.Patient.Id == patientId && x.StartTime > DateTime.Now).Count() == 0)
            {
                result.PassedValidation = false;
                result.Errors.Add("Future booking is not found for the patient!");
                return true;
            }

            return false;
        }

        private bool BookingNotDoneInPast(AddBookingRequest request, ref PdrValidationResult result)
        {
            if (!(request.StartTime > DateTime.Now && request.EndTime > request.StartTime))
            {
                result.PassedValidation = false;
                result.Errors.Add("A booking cannot be done in the past!");
                return true;
            }

            return false;
        }

        private bool BookingNotOverlaps(AddBookingRequest request, ref PdrValidationResult result)
        {
            if (!_context.Order.AsEnumerable<Order>().All(x => IsBookingAvailable(x, request)))
            {
                result.PassedValidation = false;
                result.Errors.Add("A booking overlaps with the other booking!");
                return true;
            }

            return false;
        }

        private bool IsBookingAvailable(Order existingBooking, AddBookingRequest newBooking)
        {
            bool isAvailable = true;
            if (newBooking.DoctorId == existingBooking.DoctorId && !existingBooking.IsCancelled)
            {
                // check if the appointments overlaps
                isAvailable = !(newBooking.StartTime <= existingBooking.EndTime && newBooking.EndTime >= existingBooking.StartTime);
            }

            return isAvailable;
        }
    }
}
