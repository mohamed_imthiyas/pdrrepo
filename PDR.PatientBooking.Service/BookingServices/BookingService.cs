﻿using Microsoft.EntityFrameworkCore;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Data.Models;
using PDR.PatientBooking.Service.Enums;
using PDR.PatientBooking.Service.PatientServices.Requests;
using PDR.PatientBooking.Service.PatientServices.Responses;
using PDR.PatientBooking.Service.PatientServices.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PDR.PatientBooking.Service.PatientServices
{
    public class BookingService : IBookingService
    {
        private readonly PatientBookingContext _context;
        private readonly IAddBookingRequestValidator _validator;

        public BookingService(PatientBookingContext context, IAddBookingRequestValidator validator)
        {
            _context = context;
            _validator = validator;
        }

        public void AddBooking(AddBookingRequest request)
        {
            var validationResult = _validator.ValidateRequest(request);

            if (!validationResult.PassedValidation)
            {
                throw new ArgumentException(validationResult.Errors.First());
            }

            var bookingId = new Guid();
            var bookingStartTime = request.StartTime;
            var bookingEndTime = request.EndTime;
            var bookingPatientId = request.PatientId;
            var bookingPatient = _context.Patient.FirstOrDefault(x => x.Id == request.PatientId);
            var bookingDoctorId = request.DoctorId;
            var bookingDoctor = _context.Doctor.FirstOrDefault(x => x.Id == request.DoctorId);
            var bookingSurgeryType = _context.Patient.FirstOrDefault(x => x.Id == bookingPatientId).Clinic.SurgeryType;

            var myBooking = new Order
            {
                Id = bookingId,
                StartTime = bookingStartTime,
                EndTime = bookingEndTime,
                PatientId = bookingPatientId,
                DoctorId = bookingDoctorId,
                Patient = bookingPatient,
                Doctor = bookingDoctor,
                SurgeryType = (int)bookingSurgeryType
            };

            _context.Order.AddRange(new List<Order> { myBooking });
            _context.SaveChanges();
        }

        public BookingResponse GetPatientNextAppointnemt(long patientId)
        {
            var bookings = _context.Order.Where(x => !x.IsCancelled).OrderBy(x => x.StartTime).ToList();
            var validationResult = _validator.ValidateNextAppointmentRequest(bookings, patientId);

            if (!validationResult.PassedValidation)
            {
                throw new ArgumentException(validationResult.Errors.First());
            }
            else
            {
                var booking = bookings.First(x => x.PatientId == patientId && x.StartTime > DateTime.Now);

                return new BookingResponse
                {
                    Id = booking.Id,
                    DoctorId = booking.DoctorId,
                    StartTime = booking.StartTime,
                    EndTime = booking.EndTime

                };
            }
        }

        public void CancelBooking(Guid bookingId)
        {
            var appointmentFound = _context.Order.FirstOrDefault(c => c.Id == bookingId);
            if (appointmentFound != null)
            {
                appointmentFound.IsCancelled = true;
                _context.SaveChanges();
            }
        }
    }
}
