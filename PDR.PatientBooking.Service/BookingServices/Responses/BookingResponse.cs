﻿using PDR.PatientBooking.Service.Enums;
using System;
using System.Collections.Generic;

namespace PDR.PatientBooking.Service.PatientServices.Responses
{
    public class BookingResponse
    {
        public Guid Id { get; set; }
        public long DoctorId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
