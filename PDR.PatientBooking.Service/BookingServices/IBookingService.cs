﻿using PDR.PatientBooking.Service.PatientServices.Requests;
using PDR.PatientBooking.Service.PatientServices.Responses;
using System;

namespace PDR.PatientBooking.Service.PatientServices
{
    public interface IBookingService
    {
        void AddBooking(AddBookingRequest request);
        BookingResponse GetPatientNextAppointnemt(long identificationNumber);
        void CancelBooking(Guid bookingId);
    }
}